/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states;

/**
 *
 * @author José Sousa
 */
public interface CandidaturaState {
    
    public void setCandidaturaRejeitadaState();
    
    public boolean valida();
    
}
