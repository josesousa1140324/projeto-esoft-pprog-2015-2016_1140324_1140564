/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.Exposicao;
import Model.Utilizador;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class RegistarExposicaoController {

    private CentroExposicao cExp;
    private Exposicao e;

    /**
     *
     * @param cExp
     */
    public RegistarExposicaoController(CentroExposicao cExp) {
        this.cExp = cExp;
    }

    public void novaExposicao() {
        this.e = this.cExp.getRegistoExposicao().novaExposicao();
    }

    /**
     *
     * @param titulo
     * @param descricao
     * @param dataIni
     * @param dataFim
     * @param local
     */
    public void setDados(String titulo, String descricao, String dataIni, String dataFim, String local) {
        this.e.setTitulo(titulo);
        this.e.setDescricao(descricao);
        this.e.setPeriodo(dataFim, dataFim);
        this.e.setLocal(local);
    }

    public List<Utilizador> getListaUtilizador() {
        return this.cExp.getRegistoUtilizador().getLista();
    }

    /**
     *
     * @param u
     */
    public boolean addOrganizador(Utilizador u) {
        return e.getListaOrganizador().addOrganizador(u);
    }

    public boolean validaExposicao() {
        return cExp.getRegistoExposicao().validaExposicao(e);
    }

    /**
     *
     * @param e
     */
    public boolean registaExposicao(Exposicao e) {
        return cExp.getRegistoExposicao().registaExposicao(e);
    }
}
