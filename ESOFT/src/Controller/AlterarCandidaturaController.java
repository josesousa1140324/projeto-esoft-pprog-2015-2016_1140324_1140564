/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CentroExposicao;
import Model.Exposicao;
import Model.Produto;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class AlterarCandidaturaController {

    CentroExposicao cExp;
    Exposicao expo;
    Candidatura c;
    Candidatura cClone;

    public AlterarCandidaturaController(CentroExposicao cExp) {
        this.cExp = cExp;
    }

    public List<Exposicao> getListaExposicoes(String fae) {
        return cExp.getRegistoExposicao().getListaExposicoesFAE(fae);
    }

    public void setExposicao(Exposicao expo) {
        this.expo = expo;
    }

    public List<Candidatura> getListaDeCandidaturas() {
        return expo.getListaCandidatura().getListaCandidaturaDentroDataLim();
    }

    public void setCandidatura(Candidatura c) {
        this.c = c;
        
    }

    /**
     *
     * @param empresa
     * @param morada
     * @param tlm
     * @param areaExpo
     * @param numConvites
     */
    public void setDados(String empresa, String morada, String tlm, String areaExpo, String numConvites) {
        this.cClone.setEmpresa(empresa);
        this.cClone.setmorada(morada);
        this.cClone.setTelemovel(tlm);
        this.cClone.setAreaExpo(areaExpo);
        this.cClone.setNumConvites(numConvites);
    }

    /**
     *
     * @param nomeProd
     */
    public void addNovoProduto(String nomeProd) {
        Produto p = cClone.novoProduto();
        p.setNomeProd(nomeProd);
        cClone.addProduto(p);
    }

    public void removeProcuto(String p) {

        cClone.removeProduto(p);
    }

    public void AlterarCandidatura() {
        expo.getListaCandidatura().alterarCandidatura(c, cClone);
    }

    public void registaAlteracao() {

        expo.getListaCandidatura().removeCandidatura(c);
        expo.getListaCandidatura().addCandidatura(cClone);
    }
}
