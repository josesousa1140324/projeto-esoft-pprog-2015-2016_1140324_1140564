/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.Utilizador;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class ConfirmarNovoUtilizadorController {

    private CentroExposicao cExp;
    private Utilizador u;

    public ConfirmarNovoUtilizadorController(CentroExposicao cExp) {
        this.cExp = cExp;
    }

    public List<Utilizador> getListaDeUtilizadoresPorConfirmar() {
        return this.cExp.getRegistoUtilizador().getListaDeUtilizadoresPorConfirmar();
    }

    public void setUtilizador(Utilizador u) {
        this.u = u;
    }

    public void registaUtilizador(boolean bol) {
        this.u.setConfirmacao(bol);
    }
}
