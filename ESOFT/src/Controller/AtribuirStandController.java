/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CentroExposicao;
import Model.Exposicao;
import Model.Stand;
import java.util.List;

/**
 *
 * @author José Sousa
 */
public class AtribuirStandController {
    
    private CentroExposicao centro;
    
    private Exposicao e;
    
    private Candidatura c;
    
    public AtribuirStandController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public List<Exposicao> getListaExposicoesOrganizador(String id) {
        return centro.getRegistoExposicao().getExposicoesOrganizador(id);
    }
    
    public void setExposicao(Exposicao e) {
        this.e = e;
    }
    
    public List<Candidatura> getListaCandidaturas() {
        List<Candidatura> lc = e.getListaCandidatura().getListaCandidaturas();
        return e.getListaDemonstracao().getListaDemonstracoes().get(0).getListaCandidatura().getListaCandidaturas();
    }
    
    public void setCandidatura(Candidatura c) {
        this.c = c;
    }
    
    public List<Stand> getListaStands() {
        return centro.getListaStand().getListaStands();
    }
    
    public boolean registaStand(Stand s) {
        c.setStand(s);
        return true;
    }
}
