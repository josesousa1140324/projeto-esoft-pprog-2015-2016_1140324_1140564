/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.Exposicao;
import Model.FAE;
import Model.ListaFAE;
import Model.RegistoUtilizador;
import Model.Utilizador;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class DefinirFAEController {

    private CentroExposicao cExp;
    private final RegistoUtilizador regUtil;
    private Exposicao exposicao;
    private ListaFAE listFAE;
    private List<FAE> lstFae;

    public DefinirFAEController(CentroExposicao ce) {
        this.cExp = ce;
        this.lstFae = new ArrayList<>();
        this.regUtil = ce.getRegistoUtilizador();
    }

    public List<Exposicao> getExposicoesDoOrganizador(String idOrg) {
        return this.cExp.getRegistoExposicao().getExposicoesRegistadoDoOrganizador(idOrg);
    }

    public void setExposicao(Exposicao expo) {
        exposicao = expo;
        listFAE = exposicao.getListaFAE();
        lstFae = new ArrayList<>();
    }

    public FAE addFuncionario(String idFunc) throws Exception {
        Utilizador u = this.regUtil.getUtilizadorByID(idFunc);
        FAE fae = this.listFAE.novoFAE(u);
        return fae;
    }

    public void registaFuncionario(FAE fae) {
        this.lstFae.add(fae);
    }

    public void registaDefenicaoDoFAE() {
        this.listFAE.registaDefinicaoDoFAE(this.lstFae);
    }
}
