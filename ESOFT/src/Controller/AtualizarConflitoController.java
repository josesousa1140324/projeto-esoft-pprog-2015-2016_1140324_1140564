/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CandidaturaExposicao;
import Model.CentroExposicao;
import Model.Conflito;
import Model.Exposicao;
import Model.FAE;
import Model.ListaCandidatura;
import Model.RegistoConflitos;
import Model.RegistoExposicao;
import Model.TipoConflito;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author José Sousa
 */
public class AtualizarConflitoController {
    
    private CentroExposicao centro;
    
    private RegistoExposicao ru;
    
    private Exposicao e;

    private RegistoConflitos rc;
    
    private Conflito c;
    
    private Candidatura cand;
    
    private TipoConflito tipo;
    
    private FAE fae;
    
    public AtualizarConflitoController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public List<Exposicao> getListaExposicoesFAE(String id) {
        ru = centro.getRegistoExposicao();
        return ru.getListaExposicoesFAE(id);
    }
    
    public void setExposicao(Exposicao e) {
        this.e = e;
    }
    
    public List<Conflito> getListaConflitos() {
        rc =  e.getRegistoConflitos();
        return rc.getListaConflitos();
    }
    
    public void setConflito(Conflito c) {
        this.c = c;
    }
    
    public void removeConflito() {
        rc.removeConflito(c);
    }
    
    public List<Candidatura> getListaCandidaturas() {
        return e.getListaCandidatura().getListaCandidaturas();
    }
    
    public void setCandidatura(CandidaturaExposicao cand) {
        this.cand = cand;
    }
    
    public List<TipoConflito> getListaTiposConflito() {
        return centro.getListaTipoConflito().getListaTiposConflito();
    }
    
    public void setTipoConflito(TipoConflito tipo) {
        this.tipo =  tipo;
    }
    
    public FAE getFAE(String id) {
        fae = e.getListaFAE().getFAE(id);
        return fae;
    }
    
    public void registaConflito() {
        Conflito c2 = rc.novoConflito(cand, fae, tipo);
        rc.registaConflito(c2);
    }
    
    
}
