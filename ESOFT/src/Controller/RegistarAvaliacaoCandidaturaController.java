/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Atribuicao;
import Model.Avaliacao;
import Model.Candidatura;
import Model.CentroExposicao;
import Model.Exposicao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class RegistarAvaliacaoCandidaturaController {

    private CentroExposicao cExp;
    private Exposicao expo;
    private Atribuicao at;
    private Avaliacao a;

    public RegistarAvaliacaoCandidaturaController(CentroExposicao ce) {
        this.cExp = ce;
    }

    public List<Exposicao> getListaDeExposicoes() {
        return this.cExp.getRegistoExposicao().getListaDeExposicoesPorAvaliar();
    }

    public void setExposicao(Exposicao expo) {
        this.expo = expo;
    }

    public List<Atribuicao> getListaDeCandidaturas(String idFAE) {
        return this.expo.getListaDeCandidaturasPorAvaliarById(idFAE);
    }

    public void setAtribuicao(Atribuicao at) {
        this.at = at;
    }

    public void registaAvaliacao(String avaliacao, String justificacao) {
        this.a = this.at.criaAvaliacao(avaliacao, justificacao);
    }

    public boolean registaAvaliacao() {
        return this.at.registaAvaliacao(a);
    }

}
