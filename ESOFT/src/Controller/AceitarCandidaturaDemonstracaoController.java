/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CentroExposicao;
import Model.Decisao;
import Model.Demonstracao;
import Model.Exposicao;
import Model.Organizador;
import java.util.List;

/**
 *
 * @author José Sousa
 */
public class AceitarCandidaturaDemonstracaoController {
    
    private CentroExposicao centro;
    
    private Candidatura c;
    
    public AceitarCandidaturaDemonstracaoController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public List<Exposicao> getListaExposicoesOrganizador(Organizador o) {
        return centro.getRegistoExposicao().getExposicoesOrganizador(o);
    }
    
    public List<Demonstracao> getListaDemonstracao(Exposicao e) {
        return e.getListaDemonstracao().getListaDemonstracoes();
    }
    
    public List<Candidatura> getListaCandidaturasPorDecidir(Demonstracao d) {
        return d.getListaCandidatura().getListaCandidaturasPorDecidir();
    }
    
    public void setCandidatura(Candidatura c) {
        this.c = c;
    }
    
    public void setDecisao(Decisao decisao) {
        c.setDecisao(decisao);
    }
}
