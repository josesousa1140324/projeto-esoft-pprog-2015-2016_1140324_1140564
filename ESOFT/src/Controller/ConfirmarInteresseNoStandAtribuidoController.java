/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CentroExposicao;
import Model.Representante;
import java.util.List;

/**
 *
 * @author José Sousa
 */
public class ConfirmarInteresseNoStandAtribuidoController {
    
    private CentroExposicao centro;
    
    private Candidatura c;
    
    public ConfirmarInteresseNoStandAtribuidoController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public List<Candidatura> getListaCandidaturasComStandPorConfirmar(Representante rp) {
        return centro.getRegistoExposicao().getListaCandidaturasStandPorConfirmar(rp);
    }
    
    public void setCandidatura (Candidatura c) {
        this.c = c;
    }
    
    public void setStandConfirmado() {
        c.setStandConfirmado();
    }
    
}
