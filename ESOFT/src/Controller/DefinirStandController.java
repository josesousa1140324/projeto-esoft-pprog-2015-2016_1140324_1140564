/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.ListaStand;
import Model.Stand;

/**
 *
 * @author José Sousa
 */
public class DefinirStandController {
    
    private CentroExposicao centro;
    
    private ListaStand ls;
            
    private Stand s;
    
    public DefinirStandController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public void novoStand() {
        ls = centro.getListaStand();
        s = ls.novoStand();
    }
    
    public void setDados(String nome) {
        s.setNome(nome);
    }
    
    public boolean registaStand() {
        return ls.registaStand(s);
    }
    
}
