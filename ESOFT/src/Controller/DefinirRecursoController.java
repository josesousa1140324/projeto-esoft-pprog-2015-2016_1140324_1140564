/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.ListaRecurso;
import Model.Recurso;

/**
 *
 * @author José Sousa
 */
public class DefinirRecursoController {
    
    private CentroExposicao centro;
    
    private ListaRecurso lr;
    
    private Recurso r;
    
    public DefinirRecursoController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public void novoRecurso() {
        lr = centro.getListaRecurso();
        r = lr.novoRecurso();
    }
    
    public void setRecurso(String recurso) {
        r.setRecurso(recurso);
    }
    
    public void registaRecurso() {
        lr.registaRecurso(r);
    }
}
