/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.Demonstracao;
import Model.Exposicao;
import Model.Recurso;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class CriarDemonstracaoController {

    private List<Demonstracao> lstDemo;
    private CentroExposicao cExp;
    private Exposicao expo;
    private Demonstracao demo;

    /**
     *
     * @param cExp
     */
    public CriarDemonstracaoController(CentroExposicao cExp) {
        this.cExp = cExp;
    }

    /**
     *
     * @param idOrg
     */
    public List<Exposicao> getListaDeExposicoes(String idOrg) {
        return this.cExp.getRegistoExposicao().getListaDeExposicoesByIdOrg(idOrg);
    }

    /**
     *
     * @param expo
     */
    public void setExposicao(Exposicao expo) {
        lstDemo = new ArrayList<>();
        this.expo = expo;
    }

    /**
     *
     * @param codigo
     * @param descricao
     */
    public void setDados(String codigo, String descricao) {
        this.demo = this.expo.getListaDemonstracao().novaDemonstracao();
        this.demo.setCodigo(codigo);
        this.demo.setDescricao(descricao);
    }

    /**
     *
     * @param idRecurso
     * @return
     */
    public void setRecurso(String idRecurso) {
        Recurso r = this.cExp.getListaRecurso().getRecursoById(idRecurso);
        demo.addRecurso(r);
    }

    public boolean registaDemonstracao() {// TODO - implement CriarDemonstracaoControler.validaListaDemonstracao
        this.demo.valida();
        this.validaListaDemonstracao();
        this.lstDemo.add(this.demo); 
        return true;
    }

    public boolean validaListaDemonstracao() {
        return true;
    }

    public boolean registaListaDemonstracao() {
        return this.expo.getListaDemonstracao().registaDemontracao(this.lstDemo);
    }
}
