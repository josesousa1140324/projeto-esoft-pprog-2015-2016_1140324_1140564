/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.RegistoUtilizador;
import Model.Utilizador;

/**
 *
 * @author José Sousa
 */
public class AlterarUtilizadorController {
    
    CentroExposicao centro;
    
    RegistoUtilizador ru;
    
    Utilizador u;
    
    Utilizador uClone;
    
    public AlterarUtilizadorController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public Utilizador getUtilizador(String id) {
        ru = centro.getRegistoUtilizador();
        return u = ru.getUtilizadorByID(id);
    }
    
    public void novoUtilizador() {
        uClone = ru.novoUtilizador();
    }
    
    public void setDados(String email, String nome, String username, String password) {
        uClone.setEmail(email);
        uClone.setNome(nome);
        uClone.setUsername(username);
        uClone.setPassword(nome);
        uClone.valida();
    }
    
    public boolean registaUtilizador() throws Exception {
        return ru.registaUtilizador(u, uClone);
    }
    
}
