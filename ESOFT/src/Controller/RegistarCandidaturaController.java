/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CentroExposicao;
import Model.Exposicao;
import Model.Produto;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class RegistarCandidaturaController {

    private CentroExposicao cExp;
    private Exposicao expo;
    private Candidatura c;

    /**
     *
     * @param cExp
     */
    public RegistarCandidaturaController(CentroExposicao cExp) {
        this.cExp = cExp;

    }

    public List<Exposicao> getListaDeExposicoes() {
        return this.cExp.getRegistoExposicao().getListaDeExposicoes();
    }

    /**
     *
     * @param expo
     */
    public void setExposicao(Exposicao expo) {
        this.expo = expo;
        c = this.expo.novaCandidatura();
    }

    /**
     *
     * @param empresa
     * @param morada
     * @param tlm
     * @param areaExpo
     * @param numConvites
     */
    public void setDados(String empresa, String morada, String tlm, String areaExpo, String numConvites) {
        this.c.setEmpresa(empresa);
        this.c.setmorada(morada);
        this.c.setTelemovel(tlm);
        this.c.setAreaExpo(areaExpo);
        this.c.setNumConvites(numConvites);
    }

    /**
     *
     * @param nomeProd
     */
    public void addNovoProduto(String nomeProd) {
        Produto p = c.novoProduto();
        p.setNomeProd(nomeProd);
        c.addProduto(p);
    }

    public boolean registaCandidatura() {
        return this.expo.registaCandidatura(c);
    }
}
