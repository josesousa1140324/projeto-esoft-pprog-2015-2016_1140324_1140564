/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Atribuicao;
import Model.Candidatura;
import Model.CentroExposicao;
import Model.Exposicao;
import Model.FAE;
import Model.ListaAtribuicao;
import Model.ListaMecanismoAtribuicao;
import Model.MecanismoAtribuicao1;
import Model.RegistoExposicao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class AtribuirCandidaturaFAEController {

    private CentroExposicao cExp;
    private RegistoExposicao re;
    private Exposicao expo;
    private Candidatura cand;
    private ListaMecanismoAtribuicao lm2;
    private MecanismoAtribuicao1 m;
    private ArrayList<Atribuicao> la;
    
    
    public AtribuirCandidaturaFAEController(CentroExposicao cExp) {
        this.cExp = cExp;
    }

    public List<Exposicao> getListaExposicoesOrganizador(String id) {
        re = cExp.getRegistoExposicao();
        return re.getListaDeExposicoesByIdOrg(id);
    }

    /**
     *
     * @param expo
     */
    public List<Candidatura> setExposicao(Exposicao expo) {
        this.expo = expo;
        return this.expo.getListCandidaturasPorDecidirSemFAE();
    }

    /**
     *
     * @param cand
     */
    public void setCandidatura(Candidatura cand) {
        this.cand = cand;
    }

    /**
     *
     * @param idFunc
     */
    public FAE addFuncionario(String idFunc) {
        return this.expo.getListaFAE().novoFAE(this.cExp.getRegistoUtilizador().getUtilizadorByID(idFunc));
    }

    /**
     *
     * @param fae
     */
    public boolean registaFuncionario(FAE fae) {
        return this.cand.registaFAE(fae);
    }

    public ArrayList<MecanismoAtribuicao1> getListaMecanismosAtribuicao(){
        lm2 = cExp.getListaMecanismoAtribuicao();
        return lm2.getListaMecanismosAtribuicao();
    }
    
    public void setMecanismo(MecanismoAtribuicao1 m) {
        this.m = m;
    }
    
    public ArrayList<Atribuicao> atribui() {
        la = m.atribui(expo);
        return la;
    }
    
    public void registaAtribuicoes() {
        ListaAtribuicao la2 = expo.getListaAtribuicao();
        la2.setListaAtribuicoes(la);
    }
}
