/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.Demonstracao;
import Model.Exposicao;
import java.util.List;

/**
 *
 * @author José Sousa
 */
public class DefinirDemonstracaoEfetivaController {
    
    private CentroExposicao centro;
    
    private Exposicao e;
    
    private Demonstracao d;
    
    public DefinirDemonstracaoEfetivaController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public List<Exposicao> getExposicoesOrganizador(String id) {
        return centro.getRegistoExposicao().getExposicoesOrganizador(id);
    }
    
    public void setExposicao (Exposicao e) {
        this.e = e;
    }
    
    public List<Demonstracao> getListaDemonstracoes() {
        return e.getListaDemonstracao().getListaDemonstracoes();
    }
    
    public void setDemonstracao(Demonstracao d) {
        this.d = d;
    }
    
    public void setPeriodo(String dataInicio, String dataFim) {
        d.setPeriodo(dataInicio, dataFim);
    }
    
    public void setDemonstracaoEfetiva() {
        d.setDemonstracaoEfetiva();
    }
    
}
