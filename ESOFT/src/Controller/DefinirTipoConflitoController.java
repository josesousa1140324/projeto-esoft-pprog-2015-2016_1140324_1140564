/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.ListaTipoConflito;
import Model.TipoConflito;

/**
 *
 * @author José Sousa
 */
public class DefinirTipoConflitoController {
    
    private CentroExposicao centro;

    private ListaTipoConflito ltc;
    
    private TipoConflito tc;
    
    public DefinirTipoConflitoController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public void novoTipoConflito() {
       ltc = centro.getListaTipoConflito();
       ltc.novoTipoConflito();
    }
    
    public void setTipoConflito(String tipo) {
        tc.setTipo(tipo);
    }
    
    public void registaTipoConflito() {
        ltc.registaTipoConflito(tc);
    }
    
}
