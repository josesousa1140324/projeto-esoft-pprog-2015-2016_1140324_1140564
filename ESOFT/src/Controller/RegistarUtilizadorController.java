/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicao;
import Model.Utilizador;

/**
 *
 * @author Ana Silva
 */
public class RegistarUtilizadorController {

    private final CentroExposicao cExp;
    private Utilizador u;

    public RegistarUtilizadorController(CentroExposicao ce) {
        this.cExp = ce;
    }

    public void novoUtilizador() {
        this.u = this.cExp.getRegistoUtilizador().novoUtilizador();
    }

    public void setDados(String nome, String email, String user, String pwd) throws Exception {
        this.u.setNome(nome);
        this.u.setEmail(email);
        this.u.setUsername(user);
        this.u.setPassword(pwd);
        this.cExp.getRegistoUtilizador().validaUtilizador(this.u);
    }

    public void registaUtilizador() throws Exception {
        this.cExp.getRegistoUtilizador().registaUtilizador(u);
    }

}
