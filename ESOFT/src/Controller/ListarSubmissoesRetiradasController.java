/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CentroExposicao;
import Model.Exposicao;
import java.util.List;

/**
 *
 * @author José Sousa
 */
public class ListarSubmissoesRetiradasController {
    
    private CentroExposicao centro;
    
    private Exposicao e;
    
    public ListarSubmissoesRetiradasController(CentroExposicao centro) {
        this.centro = centro;
    }
    
    public List<Exposicao> getListaExposicoesOrganizador(String id) {
        return centro.getRegistoExposicao().getExposicoesOrganizador(id);
    }
    
    public void setExposicao(Exposicao e) {
        this.e = e;
    }
    
    public List<Candidatura> getListaCandidaturasRetiradas() {
        return e.getListaCandidatura().getListaCandidaturasRetiradas();
    }
    
}
