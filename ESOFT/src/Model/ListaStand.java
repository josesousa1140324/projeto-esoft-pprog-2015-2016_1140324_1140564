/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author José Sousa
 */
public class ListaStand {
    
    private List<Stand> listaStand;
    
    public ListaStand() {
        
    }
    
    
    public Stand novoStand() {
        return new Stand();
    }
    
    
    public boolean registaStand(Stand s) {
        if (s.valida() && validaStand(s)) {
            addStand(s);
            return true;
        }
        return false;
    }
        
    public boolean validaStand(Stand s) {
        return true;
    }
    
    
    public void addStand(Stand s) {
        this.listaStand.add(s);
    }
    
    public List<Stand> getListaStands() {
        return this.listaStand;
    }
}
