/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Ana Silva
 */
public class Atribuicao {

    private Avaliacao a;
    
    private Candidatura c;
    
    private FAE fae;

    public Avaliacao criaAvaliacao(String avaliacao, String justificacao) {
        Avaliacao a = new Avaliacao();
        a.setDecisao(avaliacao);
        a.setJustificacao(justificacao);
        a.valida();
        return a;

    }

    public boolean registaAvaliacao(Avaliacao a) {
        this.validaAvaliacao(a);
        this.valida();
        this.a = a;
        return true;
    }
    
    public void setFae(FAE fae) {
        this.fae = fae;
    }

    public void setCandidatura(Candidatura c) {
        this.c = c;
    }
    private boolean valida() {
        return true;
    }

    private boolean validaAvaliacao(Avaliacao a) {
        return true;
    }

}
