/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package Model;

/**
 *
 * @author Ana Silva
 */
public class Avaliacao {

    private String decisao;
    private String justificacao;
    private FAE fae;

    public void setJustificacao(String justificacao) {

        this.justificacao = justificacao;
    }
    
    public void setDecisao(String decisao) {
        this.decisao = decisao;
    }

    public FAE getFae() {
        return fae;
    }

    public boolean valida() {
        return true;
    }
    
    public boolean registaFAE(FAE fae) {
        this.fae = fae;
        return true;
    }
}
