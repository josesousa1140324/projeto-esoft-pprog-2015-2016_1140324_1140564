/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Ana Silva
 */
public class Utilizador {

    private String nome;
    private String email;
    private String user;
    private String pwd;
    private boolean confirmado;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return user;
    }

    public void setUsername(String user) {
        this.user = user;
    }

    public String getPassword() {
        return pwd;
    }

    public void setPassword(String pwd) {
        this.pwd = pwd;
    }

    public boolean isConfirmado() {
        return confirmado;
    }

    /**
     *
     * @param bol
     */
    public void setConfirmacao(boolean bol) {
        confirmado = bol;
    }

    public boolean valida() {
        String erros = "";
        if (user.isEmpty()) {
            erros += " -> Deve inserir um user name!\n";
        }
        if (pwd.isEmpty()) {
            erros += " -> Deve inserir um palavra-chave!\n";
        }
        if (email.isEmpty()) {
            erros += " -> Deve inserir um email!\n";
        }
        if (!erros.isEmpty()) {
            return false;
            //throw new Exception(erros);
        }
        return true;
    }

}
