/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Ana Silva
 */
public class Expositor {

    private String empresa;
    private String morada;
    private String tlm;

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public void setTlm(String tlm) {
        this.tlm = tlm;
    } 
}
