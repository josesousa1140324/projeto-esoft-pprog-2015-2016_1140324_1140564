/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author José Sousa
 */
public class ListaAtribuicao {
    
    private ArrayList<Atribuicao> listaAtribuicoes;
    
    public ListaAtribuicao() {
        this.listaAtribuicoes = new ArrayList();
    }
    
    public Atribuicao novaAtribuicao() {
        return new Atribuicao();
    }
    
    public void setListaAtribuicoes(ArrayList<Atribuicao> la) {
        this.listaAtribuicoes = la;
    }
}
