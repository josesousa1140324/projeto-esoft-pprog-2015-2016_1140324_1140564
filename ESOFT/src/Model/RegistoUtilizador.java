/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class RegistoUtilizador {

    private List<Utilizador> lstUtil;

    public RegistoUtilizador() {

        this.lstUtil = new ArrayList<>();
    }

    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    public boolean registaUtilizador(Utilizador u) throws Exception {
        if (this.validaUtilizador(u)) {
            return addUtilizador(u);
        } else {
            return false;
        }
    }
    
    public boolean registaUtilizador(Utilizador u , Utilizador uClone) throws Exception {
        if (this.validaUtilizador(uClone)) {
            removeUtilizador(u);
            addUtilizador(uClone);
            return true;
        }
        return false;
    }
    
    public void removeUtilizador(Utilizador u) {
        this.lstUtil.remove(u);
    }

    public boolean validaUtilizador(Utilizador u) throws Exception {
        u.valida();
        if (this.getUtilizadorByID(u.getUsername()) != null) {
            throw new Exception("User name já existe!");
        }
        if (this.getUtilizadorByEmail(u.getEmail()) != null) {
            throw new Exception("Email já existe!");
        }
        return true;
    }

    private boolean addUtilizador(Utilizador u) {
        return this.lstUtil.add(u);
    }

    public Utilizador getUtilizadorByID(String strId) {
        for (Utilizador u : this.lstUtil) {
            if (u.getUsername().equals(strId)) {
                return u;
            }
        }
        return null;
    }

    public Utilizador getUtilizadorByEmail(String strEmail) {
        for (Utilizador u : this.lstUtil) {
            if (u.getEmail().equals(strEmail)) {
                return u;
            }
        }
        return null;
    }

    public List<Utilizador> getLista() {
        return this.lstUtil;
    }

    public List<Utilizador> getListaDeUtilizadoresPorConfirmar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
