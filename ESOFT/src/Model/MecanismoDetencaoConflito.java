/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class MecanismoDetencaoConflito {

    Exposicao Exp;
    RegistoConflitos rgConf;

    public void detectConflitos() {
        List<FAE> lstFae = Exp.getListaFAE().getLista();
        List<Candidatura> lstCan = Exp.getListaCandidatura().getListaCandidaturas();
        for (FAE n : lstFae) {
            for (Candidatura c : lstCan) {
                rgConf.registaConflito(n, c); 
            }
        }

    }

    private boolean isConflito(FAE f, Candidatura c) {
        return true;
    }

}
