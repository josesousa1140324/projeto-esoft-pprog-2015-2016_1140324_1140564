/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class ListaRecurso {

    private List<Recurso> lsRecurso;

    public Recurso getRecursoById(String idRecurso) {
        return lsRecurso.get(0);
    }
    
    public Recurso novoRecurso() {
        return new Recurso();
    }

    public void registaRecurso(Recurso r) {
        if (r.valida() & this.validaRecurso(r)) {
            addRecurso(r);
        }
    }
    
    public boolean validaRecurso(Recurso r) {
        return true;
    }
    
    public void addRecurso(Recurso r) {
        lsRecurso.add(r);
    }
}
