/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class ListaFAE {

    List<FAE> lstFae;

    public ListaFAE() {
        this.lstFae = new ArrayList<>();
    }

    public FAE novoFAE(Utilizador u) {
        FAE fae = new FAE(u);
        fae.valida();
        this.validaFAE(fae);
        return fae;
    }

    private boolean validaFAE(FAE fae) {
        return true;
    }

    public void registaDefinicaoDoFAE(List<FAE> lstFae) {
        this.lstFae = new ArrayList<>(lstFae);
    }

    public List<FAE> getLista() {
        return lstFae;
    }

    public void setLista(List<FAE> lstFae) {
        this.lstFae = lstFae;
    }
    
    public FAE getFAE(String id) {
        return this.lstFae.get(0);
    }

}
