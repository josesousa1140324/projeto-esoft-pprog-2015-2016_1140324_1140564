/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;
import states.CandidaturaCriadaState;
import states.CandidaturaState;

/**
 *
 * @author Ana Silva
 */
public abstract class Candidatura {

    private Avaliacao dec;
    private Expositor exptor;
    private String areaExpo;
    private String numConvites;
    private List<Produto> lstProduto;
    private CandidaturaState c_state;
    private Decisao decisao;
    private Stand stand;

    public Candidatura() {
        lstProduto = new ArrayList<>();
        exptor = new Expositor();
    }


    public Avaliacao registaAvaliacao(String decisao, String justificacao) {
        dec = new Avaliacao();
        dec.setDecisao(decisao);
        dec.setJustificacao(justificacao);
        dec.valida();
        return null;
    }

    boolean validaDecisao() {
        return true;
    }

    public boolean registaFAE(FAE fae) {
        return this.dec.registaFAE(fae);
    }

    public void setDec(Avaliacao dec) {
        this.dec = dec;
    }

    public void setExptor(Expositor exptor) {
        this.exptor = exptor;
    }

    public void setAreaExpo(String areaExpo) {
        this.areaExpo = areaExpo;
    }

    public void setNumConvites(String numConvites) {
        this.numConvites = numConvites;
    }

    public void setEmpresa(String Empresa) {
        this.exptor.setEmpresa(Empresa);
    }

    public void setmorada(String morada) {
        this.exptor.setMorada(morada);
    }

    public void setTelemovel(String Tlm) {
        this.exptor.setTlm(Tlm);
    }

    public Produto novoProduto() {
        return new Produto();
    }

    public void addProduto(Produto p) {
        p.valida();
        this.validaProduto();
        this.lstProduto.add(p);
    }

    boolean setState(CandidaturaCriadaState c_state) {
        this.c_state = c_state;
        return true;
    }

    private boolean validaProduto() {
        return true;
    }

    public boolean valida() {
        return true;
    }
    
    public void removeProduto(String p) {
        this.lstProduto.remove(p);
    }
    
    public void setDecisao(Decisao decisao) {
        this.decisao = decisao;
        this.c_state.setCandidaturaRejeitadaState();
        
 
   }
    
    public void setStand(Stand s) {
        this.stand = s;
    }
    
    public void setStandConfirmado() {
        
    }
}
