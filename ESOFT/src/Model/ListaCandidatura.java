/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class ListaCandidatura {

    private List<Candidatura> listaCandidaturas;

    public ListaCandidatura(List<Candidatura> listaCandidaturas) {
        this.listaCandidaturas = listaCandidaturas;
    }

    public List<Candidatura> getListaCandidaturas() {
        return listaCandidaturas;
    }

    public void setListaCandidaturas(List<Candidatura> listaCandidaturas) {
        this.listaCandidaturas = listaCandidaturas;
    }

    public List<Candidatura> getListaCandidaturaDentroDataLim() {
        return listaCandidaturas;
    }

    public void alterarCandidatura(Candidatura c, Candidatura cClone) {
        ListaCandidatura lcClone = new ListaCandidatura(listaCandidaturas);
        lcClone.removeCandidatura(c);
        lcClone.addCandidatura(cClone);

    }

    public void addCandidatura(Candidatura cClone) {
        listaCandidaturas.add(cClone);

    }

    public boolean addCandidatura() {
        return false;
    }

    public void removeCandidatura(Candidatura c) {
        listaCandidaturas.remove(c);
    }
    
    public List<Candidatura> getListaCandidaturasPorDecidir() {
        return new ArrayList();
    }

    public List<Candidatura> getListaCandidaturasRetiradas() {
        return new ArrayList();
    }
}
