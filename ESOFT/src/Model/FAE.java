/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Ana Silva
 */
public class FAE {

    private Utilizador m_utilizador;

    public FAE(Utilizador u) {
        this.setUtilizador(u);
    }

    private void setUtilizador(Utilizador u) {

        m_utilizador = u;
    }

    public Utilizador getUtilizador() {
        return this.m_utilizador;
    }

    public boolean valida()  {
        if (this.m_utilizador != null) {
            return false;
        }
        this.m_utilizador.valida();
        return true;
    }
}
