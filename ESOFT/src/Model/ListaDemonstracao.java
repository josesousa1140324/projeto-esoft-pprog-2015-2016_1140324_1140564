/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class ListaDemonstracao {

    private List<Demonstracao> lstDemo;

    public Demonstracao novaDemonstracao() {
        return new Demonstracao();
    }

    public boolean registaDemontracao(List<Demonstracao> lstDemo) {
        for (Demonstracao demo : lstDemo) {
            if (!this.validaDemonstracao(demo)) {
                return false;
            }
        }
        this.addListaDemonstracao(lstDemo);
        return true;
    }

    private boolean validaDemonstracao(Demonstracao demo) {
        return true;
    }

    private void addListaDemonstracao(List<Demonstracao> lstDemo) {
        this.lstDemo.addAll(lstDemo);
    }
    
    public List<Demonstracao> getListaDemonstracoes() {
        return this.lstDemo;
    }

}
