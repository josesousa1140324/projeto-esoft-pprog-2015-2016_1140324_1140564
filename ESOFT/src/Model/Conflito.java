/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author José Sousa
 */
public class Conflito {

    private FAE fae;

    private Candidatura cand;

    private TipoConflito tipo;

    public Conflito(Candidatura cand, FAE fae, TipoConflito tipo) {
        this.fae = fae;
        this.cand = cand;
        this.tipo = tipo;
    }

    public Conflito() {

    }
 
 public   Conflito(FAE f, Candidatura c) {
        this.fae = f;
        this.cand = c;
    }

    public FAE getFae() {
        return fae;
    }

    public void setFae(FAE fae) {
        this.fae = fae;
    }

    public Candidatura getCand() {
        return cand;
    }

    public void setCand(Candidatura cand) {
        this.cand = cand;
    }

    public TipoConflito getTipo() {
        return tipo;
    }

    public void setTipo(TipoConflito tipo) {
        this.tipo = tipo;
    }

    public boolean valida() {
        return true;
    }

}
