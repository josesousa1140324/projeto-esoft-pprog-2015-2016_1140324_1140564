/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import java.util.ArrayList;
import java.util.List;
import states.ExposicaoCriadaState;

/**
 *
 * @author Ana Silva
 */
public class Exposicao {

    private String titulo;
    private String textoDescritivo;
    private String dataInicio;
    private String dataFim;
    private Local local;
    //  private List<Organizador> lstOrganizador;
    private List<FAE> lstFae;
    private List<Candidatura> lstCandidaturasDecididas;
    private List<Candidatura> lstCandidaturasPorDecidir;
    private List<Atribuicao> lstAt;
    // private List<Demonstracao> lstDemo;
    private ListaOrganizador listOrganizador;
    private ListaFAE listFAE;
    private ListaDemonstracao lstDemo;
    private ListaAtribuicao listaAtribuicao;
    private RegistoConflitos registoConflitos;
    private ListaCandidatura listaCandidatura;
    private ExposicaoCriadaState exp_state;

    public Exposicao() {
        this.listOrganizador = new ListaOrganizador();
        this.lstAt = new ArrayList<>();
        this.local = new Local();
        this.lstDemo = new ListaDemonstracao();
    }

    public Exposicao(String titulo, String textoDescritivo, String dataInicio, String dataFim, String local, List<Organizador> lstOrganizador) {
        this.titulo = titulo;
        this.textoDescritivo = textoDescritivo;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.local.setNome(local);
        this.listOrganizador.setLista(lstOrganizador);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTextoDescritivo() {
        return textoDescritivo;
    }

    public void setDescricao(String textoDescritivo) {
        this.textoDescritivo = textoDescritivo;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = new Local(local);
    }

    public ListaOrganizador getListaOrganizador() {
        return this.listOrganizador;
    }

    public List<Candidatura> getLstCandidaturasDecididas() {
        return lstCandidaturasDecididas;
    }

    public void setLstCandidaturasDecididas(List<Candidatura> lstCandidaturasDecididas) {
        this.lstCandidaturasDecididas = lstCandidaturasDecididas;
    }

    public List<Candidatura> getLstCandidaturasPorDecidir() {
        return lstCandidaturasPorDecidir;
    }

    public void setLstCandidaturasPorDecidir(List<Candidatura> lstCandidaturasPorDecidir) {
        this.lstCandidaturasPorDecidir = lstCandidaturasPorDecidir;
    }

    public List<Atribuicao> getListaDeCandidaturasPorAvaliarById(String idFAE) {
        return this.lstAt;
    }

    public boolean registaListaCandidaturasDecidadas(List<Candidatura> lstCand) {
        for (Candidatura c : lstCand) {
            if (this.validaCandidaturaDecida(c)) {
                this.lstCandidaturasPorDecidir.remove(c);
                this.lstCandidaturasDecididas.add(c);
            }
        }
        return true;
    }

    private boolean validaCandidaturaDecida(Candidatura cand) {
        return cand.validaDecisao();
    }

    public void setPeriodo(String dataFim, String dataFim0) {
        this.setDataFim(dataFim);
        setDataInicio(dataInicio);
    }

    boolean valida() {
        return true;
    }

    public List<Candidatura> getListCandidaturasPorDecidirSemFAE() {
        return this.lstCandidaturasPorDecidir;
    }

    public Candidatura novaCandidatura() {
        return new CandidaturaExposicao();
    }

    public boolean registaCandidatura(Candidatura c) {
        c.valida();
        return this.lstCandidaturasPorDecidir.add(c);
    }

    public ListaFAE getListaFAE() {
        return listFAE;
    }

    public ListaDemonstracao getListaDemonstracao() {
        return lstDemo;
    }

    public ListaAtribuicao getListaAtribuicao() {
        return this.listaAtribuicao;
    }

    public RegistoConflitos getRegistoConflitos() {
        return this.registoConflitos;
    }

    public ListaCandidatura getListaCandidatura() {
        return this.listaCandidatura;
    }

    public boolean setExposicaoState() {
        exp_state = new ExposicaoCriadaState(this);
        return this.exp_state.SetCriadoDefinido();
    }
}
