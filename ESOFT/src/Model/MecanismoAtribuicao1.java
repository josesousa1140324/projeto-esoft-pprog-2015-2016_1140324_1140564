/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author José Sousa
 */
public class MecanismoAtribuicao1 implements MecanismoAtribuicao {

    public MecanismoAtribuicao1() {

    }

    @Override
    public ArrayList<Atribuicao> atribui(Exposicao e) {
        ArrayList<Atribuicao> la = new ArrayList();
        for (int i = 0; i < 10; i++) {
            ListaAtribuicao la2 = e.getListaAtribuicao();
            Atribuicao a = la2.novaAtribuicao();
            a.setFae(e.getListaFAE().getLista().get(i));
            a.setCandidatura(new CandidaturaExposicao());
            la.add(a);
        }
        return la;
    }
}
