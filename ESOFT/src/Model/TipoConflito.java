/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author José Sousa
 */
public class TipoConflito {

    private String tipo;
    private MecanismoDetencaoConflito mc;

    public TipoConflito(String tipo) {
        this.tipo = tipo;
    }

    public TipoConflito() {

    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean valida() {
        return true;
    }

    public MecanismoDetencaoConflito getMecanismoDetencaoConflito() {
        return mc;
    }

}
