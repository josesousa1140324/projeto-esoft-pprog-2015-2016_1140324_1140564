/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author José Sousa
 */
public class ListaTipoConflito {
    
    ArrayList<TipoConflito> listaTiposConflito;

    public ListaTipoConflito(ArrayList<TipoConflito> listaTiposConflito) {
        this.listaTiposConflito = listaTiposConflito;
    }

    public ArrayList<TipoConflito> getListaTiposConflito() {
        return listaTiposConflito;
    }

    public void setListaTiposConflito(ArrayList<TipoConflito>  listaTiposConflito) {
        this.listaTiposConflito = listaTiposConflito;
    }
    
    public TipoConflito novoTipoConflito() {
        return new TipoConflito();
    }
    
    public boolean registaTipoConflito(TipoConflito tc) {
        if (tc.valida() && validaTipoConflito(tc)){
            addTipoConflito(tc);
            return true;
        }
        return false;
    }
    
    public boolean validaTipoConflito(TipoConflito tc) {
        return true;
    }
    
    public void addTipoConflito(TipoConflito tc) {
        this.listaTiposConflito.add(tc);
    }
    
}
