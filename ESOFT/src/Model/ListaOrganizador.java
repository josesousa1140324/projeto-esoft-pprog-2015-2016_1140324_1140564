/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class ListaOrganizador {

    List<Organizador> lstOrg;

    public ListaOrganizador() {
        lstOrg = new ArrayList<>();
    }

    void setLista(List<Organizador> lstOrganizador) {
        lstOrganizador.clear();
        lstOrganizador.addAll(lstOrganizador);
    }

    public List<Organizador> getLstOrganizador() {
        return lstOrg;
    }

    public void setLstOrganizador(List<Organizador> lstOrganizador) {
        this.lstOrg = lstOrganizador;
    }

    private List<Organizador> getListOrganizador() {
        return this.lstOrg;
    }

    public Organizador getOrganizadorByID(String IdOrg) {
        for (Organizador u : this.lstOrg) {
            if (u.getUtilizador().getUsername().equals(IdOrg)) {
                return u;
            }
        }
        return null;
    }

    public boolean hasOrganizador(String IdOrg) {
        return this.getOrganizadorByID(IdOrg) != null;
    }

    public boolean addOrganizador(Utilizador u) {
        Organizador o = new Organizador(); 
        o.setUtilizador(u);
        o.valida();
        this.validaOrganizador();
        return this.lstOrg.add(o);

    }

    private boolean validaOrganizador() {
        return true;
    }

}
