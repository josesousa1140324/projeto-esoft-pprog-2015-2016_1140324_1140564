/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class RegistoExposicao {

    private List<Exposicao> lsExposicao;

    public boolean validaExposicao(Exposicao e) {
        return e.valida();
    }

    public RegistoExposicao() {
        this.lsExposicao = new ArrayList<>();

    }

    public boolean registaExposicao(Exposicao e) {
        e.valida();
        e.setExposicaoState();
        return this.lsExposicao.add(e);
    }

    public List<Exposicao> getListaDeExposicoesByIdOrg(String idOrg) {
        return this.lsExposicao;
    }

    public List<Exposicao> getListaExposicoesFAE(String idOrg) {
        return this.lsExposicao;
    }

    public List<Exposicao> getListaDeExposicoes() {
        return this.lsExposicao;
    }

    public List<Exposicao> getExposicoesRegistadoDoOrganizador(String idOrg) {
        List<Exposicao> ls = new ArrayList<>();
        for (Exposicao exposicao : this.lsExposicao) {
            if (exposicao.getListaOrganizador().hasOrganizador(idOrg)) {
                ls.add(exposicao);
            }
        }
        return ls;
    }

    public Exposicao novaExposicao() {
        return new Exposicao();
    }

    public List<Exposicao> getListaDeExposicoesPorAvaliar() {
        return this.lsExposicao;
    }

    public List<Exposicao> getExposicoesOrganizador(Organizador o) {
        return new ArrayList();
    }
    
    public List<Exposicao> getExposicoesOrganizador(String id) {
        return new ArrayList();
    }
    
    
    public List<Candidatura> getListaCandidaturasStandPorConfirmar(Representante rp) {
        return new ArrayList();
    }
}
