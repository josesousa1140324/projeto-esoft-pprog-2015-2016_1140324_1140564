/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;
import states.DemonstracaoCriadaState;

/**
 *
 * @author Ana Silva
 */
public class Demonstracao {

    private List<Recurso> lstRecurso;
    private String codigo;
    private String descricao;
    private DemonstracaoCriadaState demo_state;
    private ListaCandidatura listaCandidatura;
    private String dataInicio;
    private String dataFim;

    public Demonstracao() {
        lstRecurso = new ArrayList<>();
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void addRecurso(Recurso r) {
        this.validaRecurso(r);
        lstRecurso.add(r);
    }

    private boolean validaRecurso(Recurso r) {
        return true;
    }

    public void valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    boolean setDemonstracaoState() {
        demo_state = new DemonstracaoCriadaState();
        return true;
    }
    
    public ListaCandidatura getListaCandidatura() {
        return this.listaCandidatura;
    }
    
    public void setPeriodo(String dataInicio, String dataFim) {
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
    }
    
    public void setDemonstracaoEfetiva() {
        
    }
}
