/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author José Sousa
 */
public class RegistoConflitos {

    private List<Conflito> listaConflitos;

    public RegistoConflitos(List<Conflito> listaConflitos) {
        this.listaConflitos = listaConflitos;
    }

    public List<Conflito> getListaConflitos() {
        return listaConflitos;
    }

    public void setListaConflitos(List<Conflito> listaConflitos) {
        this.listaConflitos = listaConflitos;
    }

    public void removeConflito(Conflito c) {
        listaConflitos.remove(c);
    }

    public Conflito novoConflito(Candidatura cand, FAE fae, TipoConflito tipo) {
        return new Conflito(cand, fae, tipo);
    }

    public void registaConflito(Conflito c2) {
        if (c2.valida() && validaConflito(c2)) {
            addConflito(c2);
        }
    }

    public void registaConflito(FAE f, Candidatura c) {
        Conflito conf;
        conf = new Conflito(f, c);

    }

    public boolean validaConflito(Conflito c2) {
        return true;
    }

    public void addConflito(Conflito c2) {
        this.listaConflitos.add(c2);
    }

}
