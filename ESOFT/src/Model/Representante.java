/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Ana Silva
 */
public class Representante {

    private Utilizador m_utilizador;

    public Representante(Utilizador u) {
        this.setUtilizador(u);
    }

    public Representante() {
        
    }
    
    private void setUtilizador(Utilizador u) {

        m_utilizador = u;
    }

    public Utilizador getUtilizador() {
        return this.m_utilizador;
    }

    public boolean valida() throws Exception {
        if (this.m_utilizador != null) {
            return false;
        }
        this.m_utilizador.valida();
        return true;
    }
}
