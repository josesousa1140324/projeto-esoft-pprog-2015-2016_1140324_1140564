/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.ListaMecanismoAtribuicao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana Silva
 */
public class CentroExposicao {

    private RegistoUtilizador regUtil;
    private RegistoExposicao regExp;
    private ListaRecurso lstRec;
    private ListaMecanismoAtribuicao listaMecanismoAtribuicao;
    private ListaTipoConflito listaConflito;
    private ListaStand listaStand;

    private ListaConflitos lstCon;

    public CentroExposicao() {
        regUtil = new RegistoUtilizador();
        regExp = new RegistoExposicao();
    }

    public RegistoUtilizador getRegistoUtilizador() {
        return this.regUtil;
    }

    public RegistoExposicao getRegistoExposicao() {
        return this.regExp;
    }

    public ListaRecurso getListaRecurso() {
        return lstRec;
    }

    public ListaMecanismoAtribuicao getListaMecanismoAtribuicao() {
        return this.listaMecanismoAtribuicao;
    }

    public ListaTipoConflito getListaTipoConflito() {
        return this.listaConflito;
    }

    public ListaConflitos getRegistoConflitos() {
        return this.lstCon;
    }
    
    public ListaStand getListaStand() {
        return this.listaStand;
    }
}
    
    
