/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AtribuirStandController;
import Model.Candidatura;
import Model.CandidaturaExposicao;
import Model.CentroExposicao;
import Model.Exposicao;
import Model.Stand;

/**
 *
 * @author José Sousa
 */
public class AtribuirStandUI {
    
    private AtribuirStandController controller;
    
    public AtribuirStandUI(CentroExposicao centro) {
        controller = new AtribuirStandController(centro);
        controller.getListaExposicoesOrganizador("teste");
        controller.setExposicao(new Exposicao());
        controller.getListaCandidaturas();
        controller.setCandidatura(new CandidaturaExposicao());
        controller.getListaStands();
        controller.registaStand(new Stand());
    }
    
}
