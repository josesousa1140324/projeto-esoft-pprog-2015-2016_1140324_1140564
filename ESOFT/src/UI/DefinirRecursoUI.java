/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.DefinirRecursoController;
import Model.CentroExposicao;

/**
 *
 * @author José Sousa
 */
public class DefinirRecursoUI {
    
    private DefinirRecursoController controller;
    
    public DefinirRecursoUI(CentroExposicao centro) {
        controller = new DefinirRecursoController(centro);
        controller.novoRecurso();
        controller.setRecurso("agua");
        controller.registaRecurso();    
    }
        
    
}
