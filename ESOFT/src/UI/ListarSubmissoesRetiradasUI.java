/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.ListarSubmissoesRetiradasController;
import Model.CentroExposicao;
import Model.Exposicao;

/**
 *
 * @author José Sousa
 */
public class ListarSubmissoesRetiradasUI {
    
    private ListarSubmissoesRetiradasController controller;
    
    public ListarSubmissoesRetiradasUI(CentroExposicao centro) {
        controller = new ListarSubmissoesRetiradasController(centro);
        controller.getListaExposicoesOrganizador("teste");
        controller.setExposicao(new Exposicao());
        controller.getListaCandidaturasRetiradas();
    }
    
}
