/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.DefinirStandController;
import Model.CentroExposicao;

/**
 *
 * @author José Sousa
 */
public class DefinirStandUI {
    
    
    private DefinirStandController controller;
    
    public DefinirStandUI(CentroExposicao centro) {
        this.controller = new DefinirStandController(centro);
        controller.novoStand();
        controller.setDados("teste");
        controller.registaStand();
    }
}
