/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.DefinirDemonstracaoEfetivaController;
import Model.CentroExposicao;
import Model.Demonstracao;
import Model.Exposicao;
import java.util.List;

/**
 *
 * @author José Sousa
 */
public class DefinirDemonstracaoEfetivaUI {

    private DefinirDemonstracaoEfetivaController controller;

    public DefinirDemonstracaoEfetivaUI(CentroExposicao centro) {
        this.controller = new DefinirDemonstracaoEfetivaController(centro);
        controller.getExposicoesOrganizador("teste");
        controller.setExposicao(new Exposicao());
        controller.getListaDemonstracoes();
        controller.setDemonstracao(new Demonstracao());
        controller.setPeriodo("inicio", "fim");
        controller.setDemonstracaoEfetiva();
    }
}
