/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.DefinirTipoConflitoController;

/**
 *
 * @author José Sousa
 */
public class DefinirTipoConflitoUI {
    
    private DefinirTipoConflitoController controller;

    public DefinirTipoConflitoUI(DefinirTipoConflitoController controller) {
        this.controller = controller;
        controller.novoTipoConflito();
        controller.setTipoConflito("nao quero");
        controller.registaTipoConflito();
    }
    
    
    
}
