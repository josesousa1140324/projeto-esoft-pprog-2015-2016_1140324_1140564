/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AceitarCandidaturaDemonstracaoController;
import Model.Candidatura;
import Model.CandidaturaDemonstracao;
import Model.CentroExposicao;
import Model.Decisao;
import Model.Demonstracao;
import Model.Exposicao;
import Model.Organizador;

/**
 *
 * @author José Sousa
 */
public class AceitarCandidaturaDemonstracaoUI {
    
    private AceitarCandidaturaDemonstracaoController controller;
    
    public AceitarCandidaturaDemonstracaoUI(CentroExposicao centro) {
       controller = new AceitarCandidaturaDemonstracaoController(centro); 
       controller.getListaExposicoesOrganizador(new Organizador());
       controller.getListaDemonstracao(new Exposicao());
       controller.getListaCandidaturasPorDecidir(new Demonstracao());
       controller.setCandidatura(new CandidaturaDemonstracao());
       controller.setDecisao(new Decisao());
    }
}
