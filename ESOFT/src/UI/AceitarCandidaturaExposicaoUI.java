/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AceitarCandidaturaExposicaoController;
import Model.Candidatura;
import Model.CandidaturaExposicao;
import Model.CentroExposicao;
import Model.Decisao;
import Model.Exposicao;
import Model.Organizador;

/**
 *
 * @author José Sousa
 */
public class AceitarCandidaturaExposicaoUI {
    
    private AceitarCandidaturaExposicaoController controller;
            
            
    public AceitarCandidaturaExposicaoUI(CentroExposicao centro) {
        
        controller = new AceitarCandidaturaExposicaoController(centro);
        controller.getListaExposicoesOrganizador(new Organizador());
        controller.getCandidaturasPorDecidir(new Exposicao());
        controller.setCandidatura(new CandidaturaExposicao());
        controller.setDecisao(new Decisao());
        
    }
    
}
