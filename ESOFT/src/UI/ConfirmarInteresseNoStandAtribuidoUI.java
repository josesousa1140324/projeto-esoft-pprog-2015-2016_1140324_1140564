/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.ConfirmarInteresseNoStandAtribuidoController;
import Model.CandidaturaExposicao;
import Model.CentroExposicao;
import Model.Representante;

/**
 *
 * @author José Sousa
 */
public class ConfirmarInteresseNoStandAtribuidoUI {
    
    ConfirmarInteresseNoStandAtribuidoController controller;
    
    public ConfirmarInteresseNoStandAtribuidoUI(CentroExposicao centro) {
        controller = new ConfirmarInteresseNoStandAtribuidoController(centro);
        controller.getListaCandidaturasComStandPorConfirmar(new Representante());
        controller.setCandidatura(new CandidaturaExposicao());
        controller.setStandConfirmado();
    }
    
}
