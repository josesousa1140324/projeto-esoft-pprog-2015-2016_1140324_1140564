/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.RegistarUtilizadorController;
import Model.CentroExposicao;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author Ana Silva
 */
public class RegistarUtilizadorUI {

    RegistarUtilizadorController ctrl6;

    public RegistarUtilizadorUI(CentroExposicao ce) {
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
            this.ctrl6 = new RegistarUtilizadorController(ce);
            this.ctrl6.novoUtilizador();
            System.out.println("Insere os dados:");
            String nome = " Ana Silva";
            String email = "11140868@isep.ipp.pt";
            String user = "i140868";
            String pwd = "123456";
            this.ctrl6.setDados(nome, email, user, pwd);
            System.out.println("Deseja guardar?(s/n) ");
            String s = rd.readLine();
            if (s.equalsIgnoreCase("s")) {
                this.ctrl6.registaUtilizador();
            }
            System.out.println("Guardado com sucesso!");
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

}
