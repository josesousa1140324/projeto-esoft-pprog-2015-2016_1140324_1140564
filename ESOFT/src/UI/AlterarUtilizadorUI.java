/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AlterarUtilizadorController;
import Model.CentroExposicao;
import Model.Utilizador;

/**
 *
 * @author José Sousa
 */
public class AlterarUtilizadorUI {
    
    private AlterarUtilizadorController controller;
    
    public AlterarUtilizadorUI(CentroExposicao centro) throws Exception {
        controller = new AlterarUtilizadorController(centro);
        Utilizador u = controller.getUtilizador("ola");
        controller.novoUtilizador();
        controller.setDados("ola", "ola", "ola", "ola");
        boolean regista = controller.registaUtilizador();
    }
    
}
