/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AtualizarConflitoController;
import Model.Candidatura;
import Model.CandidaturaExposicao;
import Model.CentroExposicao;
import Model.Conflito;
import Model.Exposicao;
import Model.TipoConflito;

/**
 *
 * @author José Sousa
 */
public class AtualizarConflitoUI {
    
    private AtualizarConflitoController controller;
    
    public AtualizarConflitoUI(CentroExposicao centro) {
        controller = new AtualizarConflitoController(centro);
        controller.getListaExposicoesFAE("ola");
        controller.setExposicao(new Exposicao());
        controller.getListaConflitos();
        controller.setConflito(new Conflito());
        controller.removeConflito();
        controller.getListaCandidaturas();
        controller.setCandidatura(new CandidaturaExposicao());
        controller.getListaTiposConflito();
        controller.setTipoConflito(new TipoConflito());
        controller.getFAE("ola");
        controller.registaConflito();
    }
    
}
