/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_esoft_;

import Model.CentroExposicao;
import UI.AtribuirCandidaturaFAEUI;
import UI.ConfirmarNovoUtilizadorUI;
import UI.DefinirFAEUI;
import UI.RegistarCandidaturaUI;
import UI.RegistarAvaliacaoCandidaturaUI;
import UI.RegistarExposicaoUI;
import UI.RegistarUtilizadorUI;

/**
 *
 * @author Ana Silva
 */
public class Projeto_ESOFT_ {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CentroExposicao cExp = new CentroExposicao();
        String val = "";
        do {
            switch (val) {
                case "1":
                    RegistarExposicaoUI uc1 = new RegistarExposicaoUI(cExp);
                    break;
                case "2":
                    DefinirFAEUI uc2 = new DefinirFAEUI(cExp);
                    break;
                case "3":
                    AtribuirCandidaturaFAEUI uc3 = new AtribuirCandidaturaFAEUI(cExp);
                    break;
                case "4":
                    RegistarAvaliacaoCandidaturaUI uc4 = new RegistarAvaliacaoCandidaturaUI(cExp);
                    break;
                case "5":
                    RegistarCandidaturaUI uc5 = new RegistarCandidaturaUI(cExp);
                    break;
                case "6":
                    RegistarUtilizadorUI uc6 = new RegistarUtilizadorUI(cExp);
                    break;
                case "7":
                    ConfirmarNovoUtilizadorUI uc7 = new ConfirmarNovoUtilizadorUI(cExp);
                    break;
            }
        } while (!"0".equals(val));

    }

}
